\contentsline {chapter}{\numberline {1}Introduction}{3}
\contentsline {section}{\numberline {1.1}Overview}{3}
\contentsline {section}{\numberline {1.2}The S\textsc {toch}S\textsc {im}{} algorithm}{3}
\contentsline {section}{\numberline {1.3}Multistate molecules}{4}
\contentsline {chapter}{\numberline {2}The configuration files}{6}
\contentsline {section}{\numberline {2.1}The main configuration file (\texttt {STCHSTC.INI})}{6}
\contentsline {subsection}{\numberline {2.1.1}[Simulation Parameters]}{6}
\contentsline {subsection}{\numberline {2.1.2}[Options]}{7}
\contentsline {subsection}{\numberline {2.1.3}[File Names]}{8}
\contentsline {section}{\numberline {2.2}The complex configuration file (\texttt {COMPLEX.INI})}{8}
\contentsline {subsection}{\numberline {2.2.1}[General]}{8}
\contentsline {subsection}{\numberline {2.2.2}[Component \emph {symbol}]}{8}
\contentsline {subsection}{\numberline {2.2.3}[Complex Types]}{9}
\contentsline {subsection}{\numberline {2.2.4}[Initial Levels]}{9}
\contentsline {subsection}{\numberline {2.2.5}[Special Complex Types]}{9}
\contentsline {subsection}{\numberline {2.2.6}[Display Variable \emph {X}]}{9}
\contentsline {section}{\numberline {2.3}The configuration of reactions (\texttt {REACTION.INI})}{10}
\contentsline {subsection}{\numberline {2.3.1}[General]}{10}
\contentsline {subsection}{\numberline {2.3.2}[Reaction \emph {X}]}{10}
\contentsline {subsection}{\numberline {2.3.3}Notes on using dynamic values in this file (\texttt {REACTION.INI})}{10}
\contentsline {section}{\numberline {2.4}The configuration of dynamic values (\texttt {DYNAMIC.INI})}{10}
\contentsline {subsection}{\numberline {2.4.1}[General]}{11}
\contentsline {subsection}{\numberline {2.4.2}[Time \emph {T}]}{11}
\contentsline {section}{\numberline {2.5}Configuration of the multistate complexes (\texttt {MS\_\emph {X}.INI})}{11}
\contentsline {subsection}{\numberline {2.5.1}How multistate molecules work}{11}
\contentsline {subsection}{\numberline {2.5.2}[General]}{12}
\contentsline {subsection}{\numberline {2.5.3}[Initial States]}{12}
\contentsline {subsection}{\numberline {2.5.4}[Rapid Equilibrium \emph {X}]}{12}
\contentsline {subsection}{\numberline {2.5.5}[Reaction \emph {XD}]}{13}
\contentsline {subsection}{\numberline {2.5.6}[Display Variable \emph {X}]}{13}
\contentsline {subsection}{\numberline {2.5.7}Notes on using dynamic values in this file (\texttt {MS\_\emph {X}.INI})}{14}
\contentsline {chapter}{\numberline {3}Spatial extensions to S\textsc {toch}S\textsc {im}{}}{15}
\contentsline {section}{\numberline {3.1}Overview}{15}
\contentsline {section}{\numberline {3.2}Setting up a simulation that uses 2-D complex arrays (\texttt {STCHSTC.INI})}{16}
\contentsline {subsection}{\numberline {3.2.1}[Options]}{16}
\contentsline {subsection}{\numberline {3.2.2}[File Names]}{16}
\contentsline {section}{\numberline {3.3}Definition of complex arrays (\texttt {ARRAY.INI})}{17}
\contentsline {subsection}{\numberline {3.3.1}[General]}{17}
\contentsline {subsection}{\numberline {3.3.2}[Neighbour Sensitive Complex Types]}{17}
\contentsline {subsection}{\numberline {3.3.3}[\emph {ARRAY\_NAME}]}{17}
\contentsline {subsection}{\numberline {3.3.4}[Snapshot Variable \emph {X}]}{18}
\contentsline {section}{\numberline {3.4}Definition of neighbour-sensitive complexes (\texttt {NS\_\emph {X}.INI})}{19}
\contentsline {subsection}{\numberline {3.4.1}[General]}{19}
\contentsline {subsection}{\numberline {3.4.2}[Rapid Equilibrium \emph {X}]}{19}
\contentsline {subsection}{\numberline {3.4.3}[Reaction \emph {XD}]}{19}
\contentsline {section}{\numberline {3.5}Output of complex-array states}{20}
\contentsline {subsection}{\numberline {3.5.1}A note for StochSim 1.2 users}{20}
\contentsline {subsection}{\numberline {3.5.2}Array snapshots}{20}
\contentsline {subsubsection}{Example: How to set up the output of snapshots}{21}
\contentsline {subsection}{\numberline {3.5.3}Array dumps}{22}
\contentsline {chapter}{\numberline {4}TkS\textsc {toch}S\textsc {im}{}: user guide}{24}
\contentsline {section}{\numberline {4.1}Opening and saving a simulation}{25}
\contentsline {section}{\numberline {4.2}Setting up a simulation}{25}
\contentsline {subsection}{\numberline {4.2.1}General configuration}{25}
\contentsline {subsection}{\numberline {4.2.2}Creating elementary components of the simulation}{27}
\contentsline {subsection}{\numberline {4.2.3}Creating complexes involved in the simulation}{27}
\contentsline {subsection}{\numberline {4.2.4}Creating multistate complexes}{29}
\contentsline {subsection}{\numberline {4.2.5}Creating neighbour-sensitive complexes}{30}
\contentsline {subsection}{\numberline {4.2.6}Configuration of the bidimensional lattice}{31}
\contentsline {subsection}{\numberline {4.2.7}Creating reactions}{32}
\contentsline {section}{\numberline {4.3}Setting-up the dynamic values}{33}
\contentsline {section}{\numberline {4.4}Running a simulation}{33}
\contentsline {section}{\numberline {4.5}Visualisation of the results}{35}
\contentsline {chapter}{\numberline {5}References}{36}
\contentsline {chapter}{\numberline {A}Comparison with the Gillespie algorithm}{37}
\contentsline {section}{\numberline {A.1}Description of the algorithm}{37}
\contentsline {section}{\numberline {A.2}Comparison with the Gillespie algorithm}{38}
