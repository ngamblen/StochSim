# StochSim

STOCHSIM is a discrete, stochastic simulator. It employs a simple, novel algorithm in which enzymes
and other protein molecules are represented as individual software objects interacting according to
probabilities derived from concentrations and rate constants. Formally, it is a mesoscopic simulator,
meaning that it stores an internal representation of every molecule in the system as a unique object, but
does not simulate diffusion. The program was originally written in standard C++.

## Cite

Le Novère N., Shimizu T.S. (2001) StochSim: modelling of stochastic biomolecular processes. _Bioinformatics_ 17(6): 575-576. [doi:10.1093/bioinformatics/17.12.1226](https://doi.org/10.1093/bioinformatics/17.12.1226)

Morton-Firth C.J., Shimizu T.S., Bray D. (1999) A Free-energy-based Stochastic Simulation of the Tar Receptor Complex. _J. Mol. Biol._ 286: 1059-1074. [doi:10.1006/jmbi.1999.2535](https://doi.org/10.1006/jmbi.1999.2535)

Morton-Firth C.J., Bray D. (1998) Predicting Temporal Fluctuations in an Intracellular Signalling Pathway. _J. Theor. Biol._ 192: 117-128. [doi:10.1006/jtbi.1997.0651](https://doi.org/10.1006/jtbi.1997.0651) 

### See also

Shimizu T.S., Aksenov S.V., Bray D. (2003) A Spatially Extended Stochastic Model of the Bacterial Chemotaxis Signalling Pathway. _J. Mol. Biol._ 329(2); 291-309. [doi:10.1016/s0022-2836(03)00437-6](https://doi.org/10.1016/s0022-2836(03)00437-6)

Emonet T., Macal C.M., North M.J., Wickersham C.E., Cluzel P. (2005) AgentCell: a digital single-cell assay for bacterial chemotaxis. _Bioinformatics_ 21(11): 2714–2721. [doi:10.1093/bioinformatics/bti391](https://doi.org/10.1093/bioinformatics/bti391)

## Authors

Past developers:

Thomas Simon Shimizu (1998-2007)\
T.Shimizu@amolf.nl\
Port to Linux and spatial extensions.      

Nicolas Gambardella (1999-2007)\
nicgambarde@gmail.com\
Perl/Tk interface.

Sergej Askenov (2001-2004)\
Port to MacOSX

Michael North and Thierry Emonet (2006-2007)\
SSWrapper class for interfacing with wrapper\
applications, and the associated code

Original author of StochSim (Win32 version) and its Microsoft Windows graphical user interface:

Carl Firth (1995-1998)\
carl_aslan@yahoo.com	

The SBML1 import/export is performed by XM::Simple, written by Grant McLean.

The SBML2 export is performed by XM::Writer, written by David Meggison.

The Mersenne Twister random number generator uses a C++ implementation of Makoto Matsumoto's algorithm by Richard J. Wagner.

<img style="border:1px solid black;" src="Images/BRACH_2.gif" alt="Movement of a protein in a lattice of chemotactic receptors" height="200" />

<img src="Images/EJ20activity.gif" alt="Activity of coupled chemotactic receptors" height="200" />

<img src="Images/CaMKII-dodecamer.png" alt="Simulation of a Calcium Calmodulin Kinase II dodecamer, with various activation, binding, and phosphorylation states" height="200" />
